package itau.miti.operacao.services;

import itau.miti.operacao.persistence.Correntista;
import itau.miti.operacao.persistence.CorrentistaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OperacoesService {
    @Autowired
    private CorrentistaRepository repository;

    public Correntista abrirConta(Correntista correntista){
        return repository.save(correntista);
    }

}
