package itau.miti.operacao.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Correntista {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long numeroConta;
    private String cpf;
    private String nome;
}
