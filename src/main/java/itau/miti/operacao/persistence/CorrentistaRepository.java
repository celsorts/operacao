package itau.miti.operacao.persistence;

import org.springframework.data.repository.CrudRepository;

public interface CorrentistaRepository extends CrudRepository<Correntista, Long> {
}
