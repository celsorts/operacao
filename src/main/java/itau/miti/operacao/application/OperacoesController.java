package itau.miti.operacao.application;

import itau.miti.operacao.persistence.Correntista;
import itau.miti.operacao.services.OperacoesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class OperacoesController {
    @Autowired
    private OperacoesService operacoesService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Correntista abrirConta(@RequestBody Cliente cliente){
        Correntista correntista = cliente.getCliente();
        return operacoesService.abrirConta(correntista);
    }
}
