package itau.miti.operacao.application;

import itau.miti.operacao.persistence.Correntista;
import lombok.Data;

@Data
public class Cliente {
    private String nome;
    private String cpfNumero;

    public Correntista getCliente(){
        Correntista correntista = new Correntista();
        correntista.setNome(nome);
        correntista.setCpf(cpfNumero);

        return correntista;
    }
}
